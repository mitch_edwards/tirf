"""ThreatConnect Job App
After compiling with tcpackage, run using python ./run.py

Be default, this program will output test information in test_output.txt and counts for words and phrases in wordcloud_2.json. 
Keyword pairs are in TagKeywords_Initial.csv .

If you would like to run a wordcloud analysis, change the value of WORD_CLOUD to True instead of False. The most recent run of the wordcloud analysis can be found in wordcloud.json

"""
# standard library
import csv
import json
from datetime import datetime
from threading import Thread

# first-party
from external_app import ExternalApp  # Import default External App Class (Required)

OWNER = 'Technical Blogs and Reports'
TAG_BASE = 'https://sandbox.threatconnect.com/auth/tags/tag.xhtml?'

WORD_CLOUD=False


class App(ExternalApp):
    """External App"""

    def __init__(self, _tcex: object):
        """Initialize class properties."""
        super().__init__(_tcex)
        self.batch = None
        self.begin = datetime.now()
        self.Descriptions = []
        self.cloud_counts = {}
        self.copy = {}
        self.tag_keywords = {}
        self.around = 1
        self.tagged_count = 0
        self.ransomware_tagged = 0



        self.load_tagKeywords()
        print('[-] Loaded tag keywords')
        #print(json.dumps(self.tag_keywords))
        self.load_descriptions()
        print('[-] Loaded descriptions')
        self.load_counts()
        print(f'[-] Loaded initial objects')
        f = open('wordcloud_2.json', 'w')
        f.write(json.dumps(self.cloud_counts))
        f.close()
    def returnPos(self, phrase, desc_split):
        pos = 0
        for word in desc_split:
            if word == phrase:
                return pos 
            elif word in phrase:
                return pos 
            else:
                pos = pos+1
        if pos == 0:
            print(f'[x] Error on phrase {phrase}')
        return pos

    def returnCount(self, phrase, tag):
        count = 0
        for desc_obj in self.Descriptions:
            desc = desc_obj['description'].lower()
            link = desc_obj['weblink']
            #print(desc)
            if phrase.lower() in desc:
                count = count+1
                self.tagged_count = self.tagged_count+1
                
                filename = 'test_output.txt'
                f = open(filename, 'a')
                f.write(f'[-] Just pushed {tag} due to {phrase} to the platform with ID {id} at weblink {link}\n')
                f.close()
                        
        return count 

    def populate_wc(self, tag, index):
        copy = self.copy 
        #print(f'[-] cloud_counts[tag] = {copy[tag]}')
        phrase = list(copy[tag][index].keys())[0]
        
        print(f'\t[-] Processing wordcloud for phrase {phrase}')
        desc_count = 0.0
        for desc in self.Descriptions:
            desc_split = desc['description'].split(' ')
            pos = self.returnPos(phrase, desc_split)
            phrase_split = desc_split[pos-self.around:pos+self.around+1]
            phrase_wc = ''
            for item in phrase_split:
                phrase_wc = phrase_wc+item+' '
            #phrase_wc = phrase[:len(phrase_wc)-1]
            count = self.returnCount(phrase_wc, tag)
            self.cloud_counts[tag].append({phrase_wc:count})
            desc_count = desc_count+1
            percentage = (desc_count / len(self.Descriptions))*100
            if desc_count % 100 == 0:
                print(f'\t\t[-] A total of {str(percentage)[:4]} done with phrase {phrase}')

    def load_counts(self):
        for tag in self.tag_keywords.keys():
            for phrase in self.tag_keywords[tag]:
                count = self.returnCount(phrase, tag)
                try:
                    self.cloud_counts[tag].append({phrase : count})
                except:
                    self.cloud_counts[tag] = [{phrase:count}]
        if WORD_CLOUD:

            #Uncomment all of this for wordcloud stuff
            self.copy = self.cloud_counts     
            for tag in self.cloud_counts:
                print(f'[-] Processing wordcloud for tag {tag}')
                threads = []
                for i in range(0, len(self.copy[tag])):
                    threads.append(Thread(target = self.populate_wc, args = (tag, i)))
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                print(f'[-] Done with tag {tag}')
    def load_descriptions(self):
        self.batch: object = self.tcex.batch(self.args.tc_owner)

        # Pulls down all incidents from the Technical Blogs and Reports Organization

        parameters = {'includes': ['additional', 'attributes', 'labels', 'tags']}
        groups = self.tcex.ti.group(group_type='Incident', owner=OWNER)
        bite = []
        for group in groups.many(params=parameters):
            description = ""
            link = group["webLink"]
            id = group['id']
            try:
                for attr in group["attribute"]: # iterates through list of incident attributes
                    if attr["type"] == "Description":
                        description = attr["value"]
                        desc_obj = {
                            'description':description,
                            'weblink':link,
                            'id':id
                        }
                        self.Descriptions.append(desc_obj)
            except:
                continue

    def load_tagKeywords(self):
        with open('TagKeywords_Initial.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                tag = row[0]
                i = 1
                keywords = []
                while(True):
                    try:
                        if (row[i] != ""):
                            keywords.append(row[i])
                            
                        else:
                            break
                    except Exception as e:
                        break

                    i += 1
                self.tag_keywords[tag] = keywords
        


    def run(self) -> None:
        """Run main App logic."""
    
        percent_tagged = str((self.tagged_count / len(self.Descriptions)) *100)[0:5]
        
        print('[-] Stats: ')
        print(f'[-] Runtime: {datetime.now() - self.begin}')
        print(f'[-] Number of groups: {len(self.Descriptions)}')
        print(f'[-] Number of new tagged groups: {self.tagged_count}')
        
        print(f'[-] Percentage of groups tagged: {percent_tagged}%')
        
        
        print('[-] Done!')
